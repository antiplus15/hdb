from mongoengine import Document, StringField
from modules.base.helpers import create_app, init_db
from modules.base.admin.helpers import create_admin_app
from werkzeug.middleware.dispatcher import DispatcherMiddleware
from werkzeug.middleware.proxy_fix import ProxyFix
from config import ADMIN_PANEL_URL_PREFIX
from flask import render_template, send_from_directory, request


app = create_app(__name__)
init_db(app)


from modules import bp_news, bp_pages, bp_employees
app.register_blueprint(bp_employees, url_prefix='/employee')
app.register_blueprint(bp_news, url_prefix='/news')
app.register_blueprint(bp_pages, url_prefix='/')


admin_app, admin = create_admin_app(__name__, base_app=app)

app.wsgi_app = DispatcherMiddleware(app.wsgi_app, {ADMIN_PANEL_URL_PREFIX: admin_app.wsgi_app})
# app.wsgi_app = ProxyFix(app.wsgi_app)


@app.context_processor
def set_var():
    from modules.menu import get_menu
    variables = dict(
        menu=get_menu()
    )
    return variables


@app.before_first_request
def init():
    from modules.pages import check_index_page_is_exist
    check_index_page_is_exist()


@app.before_first_request
def print_urlmap():
    print('==================')
    print('app urlmap')
    from pprint import pprint
    pprint(app.url_map)


@app.before_first_request
def print_admin_urlmap():
    print('==================')
    print('admin_app urlmap')
    from pprint import pprint
    pprint(admin_app.url_map)


@app.errorhandler(404)
def error_404(error):
    return render_template('404.html')


# @app.route('/', methods=['GET', 'POST'])
# def hello_world():
#     if request.method == 'POST':
#         pass
#     return render_template('index.html')


# @app.route('/img/<path:filename>')
# def get_avatar(filename):
#     return send_from_directory(app.config['AVATARS_SAVE_PATH'], filename)


if __name__ == '__main__':
    app.run()
