from app import *
from modules.employees.models import *
import random
from faker import Faker
import requests
import base64


# f = Faker('uk_UA')  # does not have middle_name
f = Faker('ru_RU')

prof_categories = EmployeeProfessionCategory.objects.all()

with admin_app.app_context():
    for prof in prof_categories:
        for i in range(5 if prof.order == 1 else 15):
            empl = Employee()
            if i % 3 == 0:
                empl.last_name = f.last_name_male()
                empl.first_name = f.first_name_male()
                empl.middle_name = f.middle_name_male()
            else:
                empl.last_name = f.last_name_female()
                empl.first_name = f.first_name_female()
                empl.middle_name = f.middle_name_female()

            # resp = requests.get('https://i.pravatar.cc/300')
            # empl.imageBase64 = resp.content

            empl.date_of_birth = f.date_of_birth(minimum_age=30, maximum_age=65)
            # contact = Contact()
            # contact.phone = f.phone_number()
            # contact.email = f.email()
            # empl.contact = contact
            empl.profession = random.choice(['няня', 'повар', 'воспетка', 'сторож'])
            empl.profession_category = prof
            html = ''

            for p in f.texts():
                html += f'<p>{p}</p>'
            empl.html = html
            empl.save()
            print(empl.last_name)

