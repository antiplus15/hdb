var canvas  = $("#canvas"),
context = canvas.get(0).getContext("2d"),
$input_field = $('#imageBase64');
$init_input_data = $input_field.val() || ""
$cropped_img = $('#cropped_img');

$input_field.each(function(){
    if( $(this).val() ) {
        $('#btnDelete').show();
        $cropped_img.attr("src", $(this).val());
        $cropped_img.show();
    } else {
        $('#btnDelete').hide();
    }
});

$('#btnDelete').click(function() {
    $input_field.val("");
    $(this).hide()
    $cropped_img.hide()
    $cropped_img.attr("src", "");
    canvas.cropper('destroy');
    canvas.hide();
    $('#fileInput').val(null);
    $('#fileInput').show();
    $('#btnCrop').hide();
    $('#btnCancel').hide();
});

$('#fileInput').on( 'change', function(){
    if (this.files && this.files[0]) {
      if ( this.files[0].type.match(/^image\//) ) {
          $(this).hide();
          canvas.show();
          $('#btnCrop').show();
          $('#btnCancel').show();
        var reader = new FileReader();
        reader.onload = function(evt) {
           var img = new Image();
           img.onload = function() {
             context.canvas.height = img.height;
             context.canvas.width  = img.width;
             context.drawImage(img, 0, 0);
             var cropper = canvas.cropper({
                 viewMode: 1,
                 checkCrossOrigin: false,
                 aspectRatio: 16 / 9
             });

             $('#btnCrop').click(function() {
                // Get a string base 64 data url
                var croppedImageDataURL = canvas.cropper('getCroppedCanvas').toDataURL("image/png");
                $input_field.val(croppedImageDataURL) ;
                $cropped_img.attr("src", croppedImageDataURL);
                $cropped_img.show();
                $('#btnApply').show()
                $('#btnDelete').show();
             });

             $('#btnApply').click(function() {
                 if ($input_field.val()){
                     canvas.cropper('destroy');
                     canvas.hide();
                     $(this).hide();
                     $('#fileInput').val(null);
                     $('#btnCrop').hide();
                     $('#btnCancel').hide();
                 } else {
                     alert('Nothing selected.');
                 }
                // Get a string base 64 data url
                // var croppedImageDataURL = canvas.cropper('getCroppedCanvas').toDataURL("image/png");
                // $input_field.val(croppedImageDataURL) ;
                // $cropped_img.attr("src", croppedImageDataURL);
                $cropped_img.show();
                $('#btnDelete').show();
             });

             $('#btnCancel').click(function() {
                 canvas.cropper('destroy');
                 canvas.hide();
                 $('#fileInput').val(null);
                 $('#fileInput').show();
                 $('#btnApply').hide()
                 $('#btnCrop').hide();
                 $('#btnCancel').hide();
                 $input_field.val($init_input_data);
                 if (!$input_field.val()){
                     $('#btnDelete').hide()
                 }
                 $cropped_img.attr("src", $init_input_data)
             });

           };
           img.src = evt.target.result;
                };
        reader.readAsDataURL(this.files[0]);
      }
      else {
        alert("Invalid file type! Please select an image file.");
      }
    }
    else {
      alert('No file(s) selected.');
    }
});