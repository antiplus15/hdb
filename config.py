import os


ADMIN_PANEL_URL_PREFIX = '/admin'


class _BaseConfig:
    DEFAULT_MONGODB_SETTINGS = {'db': 'hdb', 'username': 'user', 'password': 'pass'}
    MONGODB_SETTINGS = os.environ.get("MONGODB_SETTINGS", DEFAULT_MONGODB_SETTINGS)

    FLASK_ENV = os.environ.get("FLASK_ENV", "development")
    SECRET_KEY = os.environ.get("SECRET_KEY", "test_secret_key")


class Config(_BaseConfig):
    pass


class _BaseAdminConfig(_BaseConfig):

    FLASK_ADMIN_SWATCH = 'darkly'

class AdminConfig(_BaseAdminConfig):
    pass
