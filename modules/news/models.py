from modules.base import Document
from mongoengine import StringField, DateTimeField, BooleanField, signals
from datetime import datetime
from transliterate import translit
from langdetect import detect
from werkzeug.utils import secure_filename


class News(Document):
    created_at = DateTimeField(default=datetime.now, required=True)
    publication_at = DateTimeField(default=datetime.now, required=True)
    active = BooleanField(default=True)
    url_key = StringField()
    title = StringField(required=True)
    imageBase64 = StringField()
    lead_paragraph = StringField()
    html = StringField(required=True)

    def link(self):  # TODO check
        return self.publication_at.strftime('%Y-%m-%d') + '/' + self.url_key

    @classmethod
    def pre_save(cls, sender, document, **kwargs):
        if not document.url_key:
            lang = detect(document.title)
            raw_url_key = translit(document.title, lang, reversed=True)
        else:
            raw_url_key = document.url_key
        document.url_key = secure_filename(raw_url_key.lower())


signals.pre_save.connect(News.pre_save, sender=News)
