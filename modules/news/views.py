from flask import Blueprint, render_template, abort
from jinja2 import TemplateNotFound
from .helpers import get_all_news, get_news_by_date
from datetime import datetime
from mongoengine import DoesNotExist

bp_news = Blueprint('news', __name__, template_folder='news')


@bp_news.route('/')
def news_list():
    try:
        return render_template('news/list.html', news_list=get_all_news())
    except TemplateNotFound:
        abort(404)


@bp_news.route('/<date>/<url_key>')
def news_detail(date, url_key):
    try:
        news = get_news_by_date(datetime.strptime(date, '%Y-%m-%d'), url_key=url_key)
        return render_template('news/detail.html', news=news)
    except (TemplateNotFound, DoesNotExist):
        abort(404)
