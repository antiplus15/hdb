from modules.news.models import News
from modules.base.admin import ModelView
from flask_ckeditor import CKEditorField
from modules.base.admin.widgets import ImgBase64CropperField


class NewsView(ModelView):
    def __init__(self, **kwargs):
        super().__init__(News, **kwargs)

    column_searchable_list = ('title', 'lead_paragraph')
    column_exclude_list = ('created_at', 'lead_paragraph', 'html', 'imageBase64')

    form_excluded_columns = ('created_at', )
    form_overrides = dict(
        text=CKEditorField,
        imageBase64=ImgBase64CropperField,

    )
    create_template = 'ckeditor.html'
    edit_template = 'ckeditor.html'

