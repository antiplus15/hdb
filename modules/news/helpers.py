from .models import News
from datetime import datetime, timedelta
from mongoengine import Q


def get_all_news():
    return News.objects(
        active=True,
        publication_at__lte=datetime.now()
    ).all()


def get_news_by_date(date, url_key):
    return News.objects(
        Q(publication_at__gte=date) & Q(publication_at__lt=date+timedelta(days=1)),
        url_key=url_key,
        active=True
    ).get()
