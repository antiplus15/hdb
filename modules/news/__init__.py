from .views import bp_news


def admin_init(admin, category=None):
    from modules.news.admin.views import NewsView
    admin.add_view(NewsView(name='News', category=category))

