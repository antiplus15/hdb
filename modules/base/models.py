from flask_mongoengine import Document as BaseDocument
from mongoengine import EmbeddedDocument as BaseEmbeddedDocument


class Document(BaseDocument):
    meta = {
        'abstract': True,
        'strict': False,
    }


class EmbeddedDocument(BaseEmbeddedDocument):
    meta = {
        'abstract': True,
        'strict': False,
    }
