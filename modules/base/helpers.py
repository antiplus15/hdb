from flask import Flask
from flask_mongoengine import MongoEngine
import config


def create_app(import_name):
    app = Flask(import_name, static_folder='static/app', template_folder='templates/app')
    app.config.from_object(config.Config)

    return app


def init_db(app):
    db = MongoEngine(app)
    return db
