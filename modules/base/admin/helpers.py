from flask import Flask, url_for
from flask_admin import Admin
from flask_ckeditor import CKEditor
import config
from modules.base.helpers import init_db


def create_admin_app(import_name, base_app):
    app = Flask(import_name, static_folder='static/admin', template_folder='templates/admin')
    app.config.from_object(config.AdminConfig)
    ckeditor = CKEditor(app)
    init_db(app)
    admin = Admin(app, url='/', template_mode='bootstrap4')
    init_admin_app_modules(admin)

    def base_url_for(*args, **kwargs):
        with base_app.app_context(), base_app.test_request_context():
            return url_for(*args, **kwargs)

    app.base_url_for = base_url_for  # TODO may by corecontext(use in page models)

    @app.context_processor
    def inject_variables():
        variables = dict(
            base_url_for=base_url_for
        )
        return variables

    return app, admin


def init_admin_app_modules(admin):
    from modules import menu, news, pages, employees
    CATEGORY_MENU = 'Menu'
    CATEGORY_NEWS = 'News'
    CATEGORY_PAGES = 'Pages'
    CATEGORY_EMPLOYEE = 'Employee'
    menu.admin_init(admin)  # , category=CATEGORY_MENU)
    news.admin_init(admin)  # , category=CATEGORY_NEWS)
    pages.admin_init(admin, category=CATEGORY_PAGES)
    employees.admin_init(admin, category=CATEGORY_EMPLOYEE)

