from wtforms import TextAreaField
from wtforms.widgets import TextInput
from flask import render_template


class ImgCropperWidget(TextInput):
    input_type = "text"

    def __call__(self, field, **kwargs):
        input_field = super().__call__(field, **kwargs)
        return render_template('img_cropper_widget.html', input_field=input_field)


class ImgBase64CropperField(TextAreaField):
    widget = ImgCropperWidget()
