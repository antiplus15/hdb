from .models import Employee


def get_all_employees():
    return Employee.objects(active=True).all()


def get_employee(url_key):
    return Employee.objects(active=True, url_key=url_key).get()


def get_sorted_employees():
    categories = Employee.objects(active=True).distinct('profession_category')
    categories.sort(key=lambda x: x.order)
    employees = {category: Employee.objects(active=True, profession_category=category).order_by('order_in_category').all()
                 for category in categories}
    return categories, employees
