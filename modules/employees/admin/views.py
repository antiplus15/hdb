from ..models import EmployeeProfessionCategory, Employee
from modules.base.admin import ModelView
from flask_ckeditor import CKEditorField


class EmployeeProfessionCategoryView(ModelView):
    def __init__(self, **kwargs):
        super().__init__(EmployeeProfessionCategory, **kwargs)


class EmployeeView(ModelView):
    def __init__(self, **kwargs):
        super().__init__(Employee, **kwargs)


    # column_searchable_list = ('title', )
    # column_exclude_list = ('created_at', 'html')
    #
    form_excluded_columns = ('created_at', )
    form_overrides = dict(
        html=CKEditorField,
    )
    create_template = 'ckeditor.html'
    edit_template = 'ckeditor.html'

