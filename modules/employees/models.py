from modules.base import Document, EmbeddedDocument
from mongoengine import StringField, DateTimeField, BooleanField, signals, EmbeddedDocumentField, \
    IntField, ReferenceField, DateField
from datetime import datetime
from transliterate import translit, get_available_language_codes
from langdetect import detect
from werkzeug.utils import secure_filename


class EmployeeProfessionCategory(Document):
    name = StringField(required=True, unique=True, default='Management team')
    order = IntField(required=True, unique=True, default=1)

    def __str__(self):
        return self.name


# class Contact(EmbeddedDocument):
#     phone = StringField()
#     email = StringField()


class Employee(Document):
    created_at = DateTimeField(default=datetime.now, required=True)
    active = BooleanField(default=True)
    last_name = StringField(required=True)
    first_name = StringField(required=True)
    middle_name = StringField()
    date_of_birth = DateField(default=datetime(1970, 1, 1))
    # contact = EmbeddedDocumentField(Contact)
    imageBase64 = StringField()
    profession = StringField(required=True)
    profession_category = ReferenceField(EmployeeProfessionCategory, required=True)
    order_in_category = IntField(default=10, min_value=1, max_value=1000)
    html = StringField(required=True)
    url_key = StringField(unique=True)

    @classmethod
    def pre_save(cls, sender, document, **kwargs):
        full_name = ' '.join((document.last_name, document.first_name, document.middle_name))
        suggested_lang = detect(full_name)
        lang = suggested_lang if suggested_lang in get_available_language_codes() else 'ru'
        raw_url_key = translit(full_name, lang, reversed=True)
        document.url_key = secure_filename(raw_url_key.lower())

    def __str__(self):
        return ' '.join((self.last_name, self.first_name, self.middle_name))


signals.pre_save.connect(Employee.pre_save, sender=Employee)
