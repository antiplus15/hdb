from flask import Blueprint, render_template, abort
from jinja2 import TemplateNotFound
from .helpers import get_all_employees, get_employee, get_sorted_employees
from datetime import datetime
from mongoengine import DoesNotExist

bp_employees = Blueprint('employees', __name__, template_folder='employees')


@bp_employees.route('/')
def employees_list():
    try:
        categories, employees = get_sorted_employees()
        return render_template('employees/list.html', categories=categories, employees=employees)
    except TemplateNotFound:
        abort(404)


@bp_employees.route('/<url_key>')
def employee_detail(url_key):
    try:
        employee = get_employee(url_key)
        return render_template('employees/detail.html', employee=employee)
    except (TemplateNotFound, DoesNotExist):
        abort(404)
