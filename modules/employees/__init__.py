from .views import bp_employees

def admin_init(admin, category=None):
    from .admin.views import EmployeeView, EmployeeProfessionCategoryView
    admin.add_view(EmployeeProfessionCategoryView(name='Profession category', category=category))
    admin.add_view(EmployeeView(name='Employee', category=category))
