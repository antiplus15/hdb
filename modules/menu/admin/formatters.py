from flask import Markup


def menu_formatter(view, context, model, name):

    def position(old_pos: tuple = ('',), parent_name: str = 'Menu'):
        first = old_pos + (parent_name, ' ─> ')
        not_first = old_pos + (('│' + ((len(parent_name) - 1) * ' ')) if parent_name else '', '    ')
        return first, not_first

    def sub_menu(item, is_first, f_pos, nf_pos):
        res = []
        item.sub_menu.sort(key=lambda x: x.order)
        sub_is_first = True
        for sub_item in item.sub_menu:
            f_sub_pos, nf_sub_pos = position(f_pos if is_first and sub_is_first else nf_pos, item.name)
            if not sub_is_first:
                res.append(''.join(nf_sub_pos + ('│', )))
            if hasattr(sub_item, 'sub_menu') and sub_item.sub_menu:
                res.extend(sub_menu(sub_item, sub_is_first, f_sub_pos, nf_sub_pos))
            else:
                res.append(''.join((f_sub_pos if sub_is_first else nf_sub_pos) + (sub_item.name,)))
            sub_is_first = False

        return res

    result = []
    is_first = True
    model.items.sort(key=lambda x: x.order)
    for item in model.items:
        f_pos, nf_pos = position()
        if not is_first:
            result.append(''.join(nf_pos + ('│',)))
        if item.sub_menu:
            result.extend(sub_menu(item, is_first, f_pos, nf_pos))
        else:
            result.append(''.join((f_pos if is_first else nf_pos) + (item.name,)))
        is_first = False

    return Markup(f'<pre>{"<br>".join(result)}</pre>')
