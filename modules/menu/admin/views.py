from modules.base.admin import ModelView
from .formatters import menu_formatter
from ..models import Menu
# from flask_admin.form.fields import Select2Field, Select2TagsField
# from wtforms.fields import IntegerField, StringField, SelectField


class MenuView(ModelView):

    def __init__(self, **kwargs):
        super().__init__(Menu, **kwargs)

    column_list = ('scheme',)
    column_formatters = {
        'scheme': menu_formatter
    }
    # form_subdocuments = {
    #     'items': {
    #         'form_subdocuments': {
    #             None: {
    #                 'form_overrides': dict(url=Select2Field),
    #                 'form_args': dict(url=dict(choices=[('choice_1', 'Choice 1'),
    #                                                     ('choice_2', 'Choice 2')
    #                                                     ],
    #                                            allow_blank=True,
    #                                            blank_text='None',
    #                                            validate_choice=False
    #                                            )
    #                                   )
    #             }
    #         }
    #     }
    # }









