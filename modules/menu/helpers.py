from modules.menu.models import Menu


def get_menu():
    menu = Menu.objects.first()
    if not menu:
        return []

    items = [item for item in menu.items if item.active]
    items.sort(key=lambda i: i.order)
    for item in items:
        if item.sub_menu:
            sub_menu = [sub_item for sub_item in item.sub_menu if sub_item.active]
            sub_menu.sort(key=lambda i: i.order)
            # for sub_item in sub_menu:
            #     if sub_item.sub_menu:
            #         sub_sub_menu = [sub_sub_item for sub_sub_item in sub_item.sub_menu if sub_sub_item.active]
            #         sub_sub_menu.sort(key=lambda i: i.order)

    return items
