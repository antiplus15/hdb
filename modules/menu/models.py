from modules.base import Document, EmbeddedDocument
from mongoengine import StringField, ListField, EmbeddedDocumentField, IntField, BooleanField


class MenuItemMixin:
    name = StringField(required=True)
    order = IntField(required=True)
    active = BooleanField(default=True)
    url = StringField()
    description = StringField()


class SubItem(EmbeddedDocument, MenuItemMixin):

    def __str__(self):
        return self.name


class Item(EmbeddedDocument, MenuItemMixin):
    sub_menu = ListField(EmbeddedDocumentField(SubItem))

    def __str__(self):
        return self.name


class Menu(Document):
    items = ListField(EmbeddedDocumentField(Item))

    def __str__(self):
        return self.name
