from .helpers import get_menu


def admin_init(admin, category=None):
    from modules.menu.admin.views import MenuView
    admin.add_view(MenuView(name='Menu', category=category))
