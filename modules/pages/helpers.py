from .models import IndexPage, Page


def check_index_page_is_exist():
    if not IndexPage.objects.first():
        IndexPage(title='IndexPage', html='<h1>Index page</h1>').save()


def get_index_page():
    return IndexPage.objects().get()


def get_page(url_key):
    return Page.objects(active=True, url_key=url_key).get()
