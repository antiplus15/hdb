from modules.base import Document
from mongoengine import StringField, DateTimeField, BooleanField, signals
from datetime import datetime
from transliterate import translit
from langdetect import detect
from werkzeug.utils import secure_filename
from flask import current_app


class IndexPage(Document):
    created_at = DateTimeField(default=datetime.now, required=True)
    active = BooleanField(default=True)
    title = StringField(required=True, unique=True)
    html = StringField(required=True)


class Page(Document):
    created_at = DateTimeField(default=datetime.now, required=True)
    publication_at = DateTimeField(default=datetime.now, required=True)
    active = BooleanField(default=True)
    url_key = StringField(unique=True)
    title = StringField(required=True, unique=True)
    html = StringField(required=True)

    @classmethod
    def pre_save(cls, sender, document, **kwargs):
        if not document.url_key:
            lang = detect(document.title)
            raw_url_key = translit(document.title, lang, reversed=True)
        else:
            raw_url_key = document.url_key
        document.url_key = current_app.base_url_for('pages.page', url_key=secure_filename(raw_url_key.lower()))


signals.pre_save.connect(Page.pre_save, sender=Page)
