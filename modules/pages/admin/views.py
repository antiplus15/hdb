from ..models import Page, IndexPage
from modules.base.admin import ModelView
from flask_ckeditor import CKEditorField


class IndexPageView(ModelView):
    def __init__(self, **kwargs):
        super().__init__(IndexPage, **kwargs)

    can_create = False
    can_delete = False

    column_exclude_list = ('created_at', 'html')

    form_excluded_columns = ('created_at', )
    form_overrides = dict(
        html=CKEditorField,
    )
    create_template = 'ckeditor.html'
    edit_template = 'ckeditor.html'


class PageView(ModelView):
    def __init__(self, **kwargs):
        super().__init__(Page, **kwargs)


    column_searchable_list = ('title', )
    column_exclude_list = ('created_at', 'html')

    form_excluded_columns = ('created_at', )
    form_overrides = dict(
        html=CKEditorField,
    )
    create_template = 'ckeditor.html'
    edit_template = 'ckeditor.html'

