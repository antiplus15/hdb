from flask import Blueprint, render_template, abort
from jinja2 import TemplateNotFound
from .helpers import get_index_page, get_page
from datetime import datetime
from mongoengine import DoesNotExist

bp_pages = Blueprint('pages', __name__, template_folder='pages')


@bp_pages.route('/')
def index_page():
    try:
        page = get_index_page()
        return render_template('pages/page.html', title=page.title, html=page.html)
    except (TemplateNotFound, DoesNotExist):
        abort(404)


@bp_pages.route('/<url_key>')
def page(url_key):
    try:
        page = get_page('/' + url_key)
        return render_template('pages/page.html', title=page.title, html=page.html)
    except (TemplateNotFound, DoesNotExist):
        abort(404)
