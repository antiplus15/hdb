from .views import bp_pages
from .helpers import check_index_page_is_exist


def admin_init(admin, category=None):
    from .admin.views import PageView, IndexPageView
    admin.add_view(IndexPageView(name='IndexPage', category=category))
    admin.add_view(PageView(name='Pages', category=category))

